import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import alea.MTRandom;

/**
 * Classe Simulation permet de simuler une population de lapins
 * contient :
 * un numéro de simulation pour identifier la simulation : numSimu
 * une liste de lapins qui va contenir la population de lapins : lapins
 * un entier qui décompte le tmeps écoulé de la simulation : tempsEcoule
 * un nombre de lapins de depart : nbLapinsDepart
 * un générateur de nombres aléatoires Mersenne Twister : mt
 * un sauveur d'info qui peremt d'enegistrer les étapes 
 * de la simulation dans un fichier : sauveurInfo
 *  
 */
public class Simulation{

    private int numSimu;

    private final List<Lapin> lapins;

    private int tempsEcoule;

    private final int tempsSimulation;

    private final int nbLapinsDepart;

    private final MTRandom mt;

    private SauveurInfo sauveurInfo;

    /**
     * Constructeur d'une simulation
     * @param temps entier qui détermine le temps que va durer la simulation en années
     * @param nbLapinsDepart nombre de lapins de départ
     * @param mt générateur de nombres aléatoires Mersenne Twister 
     * @param numSimu numéro de la simulation pour l'identifier
     */
    public Simulation(int temps, int nbLapinsDepart, MTRandom mt, int numSimu){
        this.numSimu = numSimu;
        this.sauveurInfo = new SauveurInfo(numSimu);
        this.tempsSimulation = temps;
        this.nbLapinsDepart = nbLapinsDepart;
        this.mt = mt;
        lapins = new LinkedList<>();
        tempsEcoule = 0;
    }

    /**
     * Permet de créer nbLapinsDeDepart lapins
     */
    public void initialisationLapinDeDepart(){
        for(int i = 0; i < nbLapinsDepart; i++){
            Lapin lapin = new Lapin(mt, 1);
            lapin.calculeChanceDeSurvie();
            lapins.add(lapin);
        }
    }

    /**
     * Permet de lancer une simulation 
     */
    public void lancerSimulation(){
        int nbFemmelles = 0;
        int nbMales = 0;
        int nbBebes;
        int nbLapinsEnVie  = 0;
        int nbLapinsMorts = 0;
        initialisationLapinDeDepart();

        System.out.println("Simulation numéro : " + numSimu);
        //déroule la simulation 
        for (tempsEcoule = 0 ; tempsEcoule < tempsSimulation; tempsEcoule++){
            List<Lapin> listeBebes = new LinkedList<>();
            boolean verifMale = verficationAuMoinsUnMale();

            //parcourt de tous les lapins
            for (Lapin lapin : lapins) {
                if (lapin.isEnVie()) {
                    nbLapinsEnVie++;

                    if (lapin.getSex() == Sex.FEMELLE) {
                        if (lapin.isMature() && verifMale) {
                            lapin.accoucher(listeBebes);
                        }
                        nbFemmelles++;
                    } else {
                        nbMales++;
                    }

                    double tmp = mt.nextDouble();
                    if (tmp > lapin.getChanceDeSurvivre()) {
                        lapin.setEnVie(false);
                    }
                    lapin.incrementerAgeAnneeLapin();
                    lapin.calculeChanceDeSurvie();

                } else {
                    nbLapinsMorts++;
                }
            }
            lapins.addAll(listeBebes);
            nbBebes = listeBebes.size();
            nbLapinsEnVie = nbLapinsEnVie + nbBebes;

            afficherInfo(nbMales, nbFemmelles, nbBebes, nbLapinsMorts, nbLapinsEnVie);
            sauveurInfo.sauverEtat(nbMales, nbFemmelles, nbBebes,
                    nbLapinsMorts, nbLapinsEnVie);

            nbFemmelles = 0;
            nbMales = 0;
            nbLapinsEnVie = 0;
            nbLapinsMorts = 0;
        }
        sauveurInfo.fermerWriter();
    }

    /**
     * Permet d'afficher les info d'une simulation à la fin de chaque année écoulée
     * @param nbMales nombre de lapins males
     * @param nbFemmelles nombre de lapins femmelles
     * @param nbBebes nombre de bébés lapins
     * @param nbLapinsMorts nombre de lapins morts
     * @param nbLapinsEnVie nombre de lapins vivants
     */
    private void afficherInfo(int nbMales, int nbFemmelles, int nbBebes,
                               int nbLapinsMorts, int nbLapinsEnVie){
        System.out.printf("Annee %d : \tNb males : %d \t\tNb femelles : %d \t\tNb bébés : %d%n",
                tempsEcoule, nbMales, nbFemmelles, nbBebes);
        System.out.println("Nb total lapins : " + lapins.size() +
                " Nb morts : " + nbLapinsMorts+ " Nb vivants : " + nbLapinsEnVie);
        System.out.println();
    }

    /**
     * Vérifie que parmi tous les lapins vivants il y en ait au moins qui soit un male en vie
     * @return true si au moins un male en vie false sinon
     */
    private boolean verficationAuMoinsUnMale() {
        boolean verifMale = false;
        for(Lapin lapin : lapins){
            if(lapin.getSex() == Sex.MALE){
                verifMale = true;
                break;
            }
        }
        return verifMale;
    }

}
