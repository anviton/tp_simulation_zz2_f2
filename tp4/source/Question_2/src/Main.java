import alea.MTRandom;


public class Main {
    /**
     * Ici on vient créer un générateur de nombre aléatoire Mersenne Twister
     * avec une graine intéressante, et l'on exécute 10 réplications
     * de notre simulation
     * @param args argument de la ligne de commande
     */
    public static void main(String[] args) {
        Simulation simulation;
        MTRandom rnd = new MTRandom();
        rnd.setSeed(new int[] {0x123, 0x234, 0x345, 0x456});
        for(int i  = 0; i < 30; i++){
            simulation = new Simulation(16, 10, rnd, i);
            simulation.lancerSimulation();
        }
    }
}

