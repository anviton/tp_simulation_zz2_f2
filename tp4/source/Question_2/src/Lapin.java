import alea.MTRandom;
import java.util.*;

/**
 * Classe lapin qui permet de gérer la vie d'un lapin
 * contient :
 * un age en année : ageAnnee
 * un age en année à partir du quel un lapin est considéré comme mature : ageAnnneeMature
 * un booléen qui permet de savoir si le lapin est en vie ou non : enVie
 * un sex male ou femelle : sex
 * une chance de survie qui permet de connaitre le taux de survie
 * du lapin chaque année : chanceDeSurvivre
 * une probabilité d'être un male ou une femelle : probMaleFemelle
 * un générateur de nombre aléatoire (Mersenne Twister) : mt
 */
public class Lapin{
    
    private int ageAnnee;

    private static final int ageAnnneeMature = 1;

    private boolean enVie;

    private Sex sex;

    private double chanceDeSurvivre;

    private final double probMaleFemelle;

    private final MTRandom mt;

    /**
     * Constructeur d'un lapin
     *
     * détermine si le lapin créé doit être un male ou une femelle
     * @param mt le générateur de nombres aléatoires Mersenne Twister
     * @param age l'age du lapin
     */
    public Lapin(MTRandom mt, int age){
        this.mt = mt;
        this.ageAnnee = age;
        enVie = true;
        probMaleFemelle = 0.5;
        chanceDeSurvivre = 0.35;
        maleOuFemelle();
    }

    /**
     * Permet de connaitre le sex du lapin
     * @return le sex du lapin
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * Permet de connaitre la chance de survie d'un lapin
     * @return la chance de survie du lapin
     */
    public double getChanceDeSurvivre() {
        return chanceDeSurvivre;
    }

    /**
     * Permet de savoir si un lapin est mature
     * @return true si l'age du lapin est supérieur à ageAnnneeMature false sinon
     */
    public boolean isMature(){
        boolean mature;
        mature = ageAnnee >= ageAnnneeMature;
        return mature;
    }

    /**
     * Permet de définir si un lapin est vivant ou mort
     * @param enVie nouvelle valeur du paramètre enVie (true = vivant; false = mrot)
     */
    public void setEnVie(boolean enVie){
        this.enVie = enVie;
    }

    /**
     * Permet de savoir si un lapin est vivant ou mort
     * @return true si le lapin est vivant false s'il est mort
     */
    public boolean isEnVie(){
        return enVie;
    }

    /**
     * Incrémente l'âge du lapin
     */
    public void incrementerAgeAnneeLapin() {
        ageAnnee++;
    }

    /**
     * Permet de déterminer le sex du lapin en utilisant la probMaleFemelle
     * (probabilité d'être un male ou une femelle)
     */
    private void maleOuFemelle(){
        double r = mt.nextDouble();
        if(r < probMaleFemelle){
            sex = Sex.MALE;
        }
        else{
            sex = Sex.FEMELLE;
        } 
    }

    /**
     * Permet de caluler les chances de survie du lapin en fonction de son âge
     */
    public void calculeChanceDeSurvie(){
        double reduction;
        if (ageAnnee < 1) 
        {
            chanceDeSurvivre = 0.35;
        } 
        else if (ageAnnee < 10)
        {
            chanceDeSurvivre = 0.6;
        } 
        else if (ageAnnee < 15)
        {
            reduction = (ageAnnee - 10) * 0.1;
            chanceDeSurvivre = 0.6 - reduction;
        }
    }

    /**
     * Permet de donner naissance à des lapins (bébés lapins),
     * en fonction du nombre de portees et détermination du nombre de lapins
     * de chaque portee
     * @param listeBebes liste ou vont être ajoutés les nouveux bébés Lapins
     */
    public void accoucher(List<Lapin> listeBebes){
        int nbBebes;
        int nbPortees = calculerNbPortees();

        for(int i = 0; i < nbPortees; i++){
            nbBebes = mt.nextInt(3, 6);
            for(int j = 0; j < nbBebes; j++){
                listeBebes.add(new Lapin(mt, 0));
            } 
        }
    }

    /**
     * Calcul le nombre de portées qu'un lapin va avoir en une année
     * @return le nombre de portées calculé
     */
    private int calculerNbPortees(){
        int nbPortees;
        double r;
        r = mt.nextDouble();
        if (r < 0.1) {
            nbPortees = 4;
        } else if (r < 0.4) {
            nbPortees = 5;
        } else if (r < 0.7) {
            nbPortees = 6;
        } else {
            nbPortees = 7;
        }
        return nbPortees;
    }   
}