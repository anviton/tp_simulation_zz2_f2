import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Classe SauveurInfo permet de sauvegarder les données d'une simulation
 * contient un BufferedWriter pour écrire dans un fichier
 */
public class SauveurInfo {

    private BufferedWriter writer;

    /**
     * Constructeur d'un SauveurInfo
     * @param numSimu numéro de la simulation dont on souhaite sauvegarder les informations
     */
    public SauveurInfo(int numSimu){
        try {
            File fichier = new File("res/Simulation" + numSimu + ".CSV");

            // Vérifie si le fichier n'existe pas
            if (!fichier.exists()) {
                fichier.createNewFile();
                System.out.println(" test" + fichier.getAbsolutePath());
            }
            FileWriter fileWriter = new FileWriter(fichier);
            writer = new BufferedWriter(fileWriter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Ecrit dans le fichier les informations sur le nombre de lapins
     * @param nbMales le nombre de lapins males
     * @param nbFemmelles le nombre de lapins femelles
     * @param nbBebes le nombre de bébés
     * @param nbLapinsMorts le nombre de lapins morts
     * @param nbLapinsEnVie le nombre de lapins en vie
     */
    public void sauverEtat(int nbMales, int nbFemmelles, int nbBebes,
                           int nbLapinsMorts, int nbLapinsEnVie) {
        try {
            writer.write(nbMales + ";" + nbFemmelles + ";" + nbBebes + ";" +
                    nbLapinsMorts + ";" + nbLapinsEnVie);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ferme le writer
     */
    public void fermerWriter(){
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


