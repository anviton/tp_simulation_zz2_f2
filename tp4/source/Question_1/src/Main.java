public class Main {

    /**
     * @brief Fonction récursive pour calculer le terme n de la suite de Fibonacci.
     *
     * @param n L'indice du terme à calculer.
     * @return La valeur du terme Fibonacci correspondant à l'indice n.
     */
    public static int fibonacci(int n) {
        if (n <= 1) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    /**
     * @brief Fonction principale pour tester le calcul de la suite de Fibonacci.
     *
     * @param args Les arguments de la ligne de commande.
     */
    public static void main(String[] args) {
        int n = 6; 
        System.out.println("Suite de Fibonacci jusqu'à " + n + " termes:");
        
        for (int i = 0; i < n; i++) {
            System.out.println(fibonacci(i) + " ");
        }
    }
}

