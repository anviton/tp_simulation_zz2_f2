## Comment utiliser le projet ⚙️

(en se plaçant dans le dossier source/Question_1 ou source/Question_2)

### Compilation

javac -d bin -sourcepath src src/Main.java

### Execution

java -Xms256m -Xmx8g -cp bin Main
