#include <stdio.h>
#include <stdlib.h>
#include "global.h"

/**
 * @brief Permet de répéter le lancer d'une piece un nombre de fois passées en paramètre 
 * et d'afficher les résultats
 * @param nb_lancers nombre de lancers de pieces
*/
void lancer_une_piece(int nb_lancers){
    int nb, i;
    int nb_faces = 0; 
    int nb_piles = 0;

    for(i = 0; i < nb_lancers; i++){
        nb = rand() % 2;
        if(nb == 0){
            nb_faces++;
        }
        else{
            nb_piles++;
        }
    }

    printf("Pour %d exp : NB faces : %d \t NB Piles %d\n", nb_lancers, nb_faces, nb_piles);
}

/**
 * @brief Permet de simuler plusieurs lancers de dés en paramétrant le nombre de faces
 * et d'afficher les résultats des lancers
 * @param nb_opp nombre de lancers
 * @param nb_faces nombre de faces du dé
*/
void lancer_de_dé(int nb_opp, int nb_faces){
    int *tab_res = (int*)malloc(nb_faces*sizeof(int));
    int res, i;

    for(i = 0; i < nb_faces; i++){
        tab_res[i] = 0;
    }

    for(i = 0; i < nb_opp; i++){
        res = (rand() % nb_faces);
        tab_res[res]++;
    }

    for(i = 0; i < nb_faces; i++){
        printf("%d : NB de faces %d = %d\n", nb_opp, i + 1, tab_res[i]);
    }

    free(tab_res);
}

/**
 * @brief Réponse de la question 4
*/
void question_4(void){
    printf(ANSI_COLOR_GREEN "\nQuestion 4 :\n\n" ANSI_COLOR_RESET);
    lancer_une_piece(10);
    lancer_une_piece(100);
    lancer_une_piece(1000);
}

/**
 * @brief Réponse de la question 5
*/
void question_5(void){
    printf(ANSI_COLOR_GREEN "\nQuestion 5 :\n\n" ANSI_COLOR_RESET);

    lancer_de_dé(10, 6);
    printf("\n");

    lancer_de_dé(100, 6);
    printf("\n");

    lancer_de_dé(1000, 6);
    printf("\n");

    lancer_de_dé(1000000, 6);
    printf("\n");
}

/**
 * @brief Réponse de la question 6
*/
void question_6(void){
    printf(ANSI_COLOR_GREEN "\nQuestion 6 :\n\n" ANSI_COLOR_RESET);
    lancer_de_dé(10, 10);
    printf("\n");

    lancer_de_dé(100, 10);
    printf("\n");

    lancer_de_dé(1000, 10);
    printf("\n");

    lancer_de_dé(1000000, 10);
    printf("\n");
}

/**
 * @brief Réponses aux questions de la partie B
*/
void partie_B(void){
    printf(ANSI_COLOR_SKYBLUE "\nPartie B :\n" ANSI_COLOR_RESET);
    question_4();
    question_5();
    question_6();
}