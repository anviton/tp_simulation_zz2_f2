#include "A-Nonlinear_generator.h"
#include "B-Model_Usual_randomness.h"
#include "C-LCGs.h"
#include "E-Tausworthe_generatoirs.h"

/**
 * @brief Appel des différentes parties qui répondent aux questions
 * Conseil : Pour faciliter la correction vous pouvez observer les réponses partie par partie 
 * en mettant en commentaire les autres.
 * @return int code de reour
 */
int main(void){
    partie_A();
    partie_B();
    partie_C();
    partie_E();
	return 0;
}