#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "global.h"
 
/**
 * @brief Linear congruential generators avec des entiers (+ test avec les 32 premiers pseudo random numbers)
*/
void intRand(void){
   int x0 = 5;
   int x, i;
   int nb_opp = 32;
   x = x0;

   for(i = 0; i < nb_opp; i++){
      x  = (5 * x + 1) % 16;
      printf("x%d = %d\n", i +1, x);
   }
}

/**
 * @brief Permet de générer et afficher des nombres pseudo aléatoires flotant à partir 
 * d'un linear congruential generators paramétrable
 * @param a élément a de l'éqation : xi+1 = ((a * x) +c) % m utlisé pour générer l'aléatoire
 * @param c élément c de l'éqation : xi+1 = ((a * x) +c) % m utlisé pour générer l'aléatoire
 * @param m élément m de l'éqation : xi+1 = ((a * x) +c) % m utlisé pour générer l'aléatoire
 * @param seed graine utilisée pour l'aléatoire
 * @param nb_opp nombre de nombres pseudo aléatoires à générer
*/
void floatRand(unsigned long int a, int c, unsigned long int m, int seed, int nb_opp){
   int i;
   double x_res;
   unsigned long int x;
   x = seed;

   for(i = 0; i < nb_opp; i++){
      x  = ((a * x) + c) % m;
      printf("x : %ld\n", x);
      x_res = (double)(x) / 16;
      printf("x%d = %.4f\n", i +1, x_res);
   }
}
/**
 * @brief Réponse à la partie 2 de la question 7 et à la question 8
*/
void question_7_partie_2_et_question_8(void){

   printf("Les paramètres a = 5\tc = 1\tm = 16\tseed = 5\tnb_opp = 32");
   floatRand(5, 1, 16, 5, 32);
   printf("\n");

   printf("Les paramètres a = 5\tc = 2\tm = 14\tseed = 9\tnb_opp = 32");
   floatRand(3, 2, 14, 9, 32);
   printf("\n");
   
   printf("Param opti : Les paramètres a = 22695477\tc = 1\t m = pow(2, 32)\tseed = 5\tnb_opp = 32");
   floatRand(22695477, 1, pow(2, 32), 5, 32);
}

/**
 * @brief Réponse aux questions de la partie C
*/
void partie_C(void){
   printf(ANSI_COLOR_SKYBLUE "\nPartie C :\n" ANSI_COLOR_RESET);
   printf(ANSI_COLOR_GREEN "\nQuestion 7 partie 1 :\n\n" ANSI_COLOR_RESET);
   intRand();
   printf(ANSI_COLOR_GREEN "\nQuestion 7 partie 2 et question 8 :\n\n" ANSI_COLOR_RESET);
   question_7_partie_2_et_question_8();
}
