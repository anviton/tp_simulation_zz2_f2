#ifndef __B_H__
#define __B_H__

#define NB_ITER 10

void question_4(void);
void question_5(void);
void question_6(void);
void lancer_de_dé(int nb_opp, int nb_faces);
void lancer_une_piece(int nb_lancers);
void partie_B(void);


#endif