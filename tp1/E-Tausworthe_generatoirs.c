#include <stdio.h>
#include <stdlib.h>
#include<stdint.h>
#include "global.h"

/**
 * @brief Permet d'afficher un entier de 4 bits
 * @param nb nombre à afficher
*/
void affich_4_bits(uint8_t nb){
    int i;
    uint8_t bit;
    for(i = 3; i >= 0; i--){
        bit = (nb >> i) & 1;
        printf("%u", bit);
    }
    printf("\n");
}

/**
 * @brief Tausworthe generator (basic 4 bits right shift register generator)
 * @param nb premier nombre à être insérer dans le générateur
 * @return on retourne le nombre "calculé"
*/
int generator(uint8_t nb){
    uint8_t bit4;
    uint8_t bit3;
    uint8_t res_xor;
    bit4 = (nb >> 0) & 1;
    bit3 = (nb >> 1) & 1;
    nb = nb >> 1;
    res_xor = (bit3 ^ bit4);
    if(res_xor){
       nb = nb | 0b1000;
    }
    return nb;
}

/**
 * @brief Dernière question avec une boucle sur le Tausworthe generator
*/
void derniere_question(void){
    uint8_t nb;
    int i;
    nb = 0b0110;

    printf("Etape 0 :\n");
    affich_4_bits(nb);
    printf("\n");

    for(i = 1; i < 101; i++){
        nb = generator(nb);
        
        printf("Etape %d :\n", i);
        affich_4_bits(nb);
        printf("\n");
    }
}

/**
 * @brief Réponse à la partie E
*/
void partie_E(void){
    printf(ANSI_COLOR_SKYBLUE "\nPartie E : \n" ANSI_COLOR_RESET);
    printf(ANSI_COLOR_GREEN "\n Dernière question:\n\n" ANSI_COLOR_RESET);
    derniere_question();
}