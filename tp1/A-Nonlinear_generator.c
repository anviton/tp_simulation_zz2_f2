#include <stdio.h>
#include "global.h"

/**
 * @brief nonlinear_generator réponse à la question 1
*/
void nonlinear_generator_question_1(void){
    int n0 = 1234;
    int n1;
    n1 = 1234 * 1234;

    printf(ANSI_COLOR_GREEN "\nQuestion 1 :\n\n" ANSI_COLOR_RESET);

    // %08d permet d'afficher un entier sur 8 digits
    printf(" 1234 * 1234 = %08d\n", n1);

    n1 = ((n0 * n0) / 100) % 10000;

    printf("n1 = %08d\n", n1);
    
}

/**
 * @brief nonlinear generator permet d'observer l'aléatoire créé à partir d'une graine (observation des cycles)
 * @param graine graine utilisée pour démarrer la génération aléatoire
*/
void nonlinear_generator(int graine){
    int n = graine;
    int i, inter;
    
    printf("La graine est %d\n", graine);
    printf("n0 = %04d\n\n", n);

    for(i=1; i < 50; i++){
        inter = n * n;
        n = ((inter) / 100) % 10000;
        printf("n%d %08d %04d\n\n", i, inter, n);
    }
    
}

/**
 * @brief Réponse à la question 2
*/
void question_2(void){
    printf(ANSI_COLOR_GREEN "\nQuestion 2 :\n\n" ANSI_COLOR_RESET);
    nonlinear_generator(1234);
    nonlinear_generator(1324);
    nonlinear_generator(4100);
    nonlinear_generator(1301);
    nonlinear_generator(3141);
}

/**
 * @brief Réponse aux questions de la partie A
*/
void partie_A(void){
    printf(ANSI_COLOR_SKYBLUE "\nPartie A : \n" ANSI_COLOR_RESET);
    nonlinear_generator_question_1();
    question_2();
}