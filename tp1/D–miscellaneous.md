# D – miscellaneous

## 11
- Matsumoto PRNGs : en C je l'ai trouvé sous le nom mt64
- "méthode" de L'Ecuyer : en C je l'ai trouvé sous le nom lfsr113

liens de recherche :
- https://github.com/Bill-Gray/prngs (Matsumoto PRNGs et autres)
- https://github.com/cmcqueen/simplerandom (L'Ecuyer et autres)
- http://simul.iro.umontreal.ca/rng/lfsr113.c (L'Ecuyer)


## 12
- TestU01
- Diehard tests

liens de recherche :
- http://simul.iro.umontreal.ca/testu01/tu01.html (TestU01)
- https://github.com/umontreal-simul/TestU01-2009/ (TestU01)
- https://github.com/seehuhn/dieharder (Dieharder)
- https://en.wikipedia.org/wiki/Diehard_tests (Diehard tests) 


