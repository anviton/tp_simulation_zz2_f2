#include <math.h>

#include "matsumoto.h"
#include "global.h"
#include "question1.h"


/**
 * @brief permet d'estimer la valeur de Pi en comptant le nombre de pts qui appartiennent à 
 * un quart de cercle
 * @param nb_points nombre de points utilisés pour simuler pi
*/
double simPi(int nb_points){
    double x, y, pi;
    int nb_pts_cercle = 0, i;
    for(i = 0; i < nb_points; i++){
        x = genrand_real1();
        y = genrand_real1();

        if(x * x + y * y <= 1){
            nb_pts_cercle++;
        }
    }

    pi = 4 * ((double)nb_pts_cercle / nb_points);
    return pi;
}

/**
 * @brief reponse à la question 1
*/
void question1(void){
    double pi;

    printf(ANSI_COLOR_SKYBLUE "\nQuestion 1 : \n" ANSI_COLOR_RESET);

    pi = simPi(1000);
    printf("Estimation pour 1 000 points : PI = %10.8f\n", pi);

    pi = simPi(1000000);
    printf("Estimation pour 1 000 000 points : PI = %10.8f\n", pi);

    pi = simPi(1000000000);
    printf("Estimation pour 1 000 000 000 points : PI = %10.8f\n", pi);
}


