#include <math.h>
#include <stdio.h>

#include "question1.h"
#include "global.h"

/**
 * @brief calul la moyenne de Pi, l'erreur absolue et l'erreur relative par rapport à M_PI
 *  pour nbexperiences et '1 000, 1 000 000 et 1 000 000 000' simulations
 * @param nb_experiences nombre d'experiences
*/
void moyennePi(int nb_experiences){
    double somme_pi_calcules[3] = {0.};
    double moyennes[3] = {0.};
    double erreur_absolue[3], erreur_relative[3];
    int i, j;
    int tab_nb_points[3] = {1000, 10000000, 1000000000};

    for(i = 0; i < 3; i++){
        for(j = 0; j < nb_experiences; j++){
            somme_pi_calcules[i] = somme_pi_calcules[i] + simPi(tab_nb_points[i]);
        }

        moyennes[i] = somme_pi_calcules[i] / nb_experiences;
        erreur_absolue[i] = fabs(moyennes[i] - M_PI);
        erreur_relative[i] = erreur_absolue[i] / M_PI;

    }

    for(i = 0; i < 3; i++){
        printf("Avec %d points et %d experiences, on a moyenne pi = %f\n", tab_nb_points[i], 
        nb_experiences, moyennes[i]);
        printf("Erreur absolue = %f\n", erreur_absolue[i]);
        printf("Erreur relative = %f\n\n", erreur_relative[i]);
    }
}

/**
 * @brief réponse à la question 2
*/
void question2(void){

    printf(ANSI_COLOR_SKYBLUE "\nQuestion 2 : \n" ANSI_COLOR_RESET);

    moyennePi(10);
    
    //moyennePi(20);

    moyennePi(30);

    moyennePi(40);
}