#include <stdio.h>
#include <math.h>

#include "global.h"
#include "question1.h"
#include "question3.h"

/**
 * valeurs d'alpha pour le calcul de l'intervalle de confiance
*/
const double alpha_valeurs[] = {
    63.657, 9.925, 5.841, 4.604, 4.032, 3.707, 3.499, 3.355, 3.250, 3.169,
    3.106, 3.055, 3.012, 2.977, 2.947, 2.921, 2.898, 2.878, 2.861, 2.845,
    2.831, 2.819, 2.807, 2.797, 2.787, 2.779, 2.771, 2.763, 2.756, 2.750
};

/**
 * @brief calcul l'estimateur de la variance
 * @param moyenne moyenne de la série de données passée en paramètre
 * @param nb_donnees taille de la série passée en paramètre
 * @return l'estimateur calculé
*/
double calulerEstimateurVariance(double moyenne, double *donnes, double nb_donness){
    double somme = 0;
    for (int i = 0; i < nb_donness; i++)
    {
        somme = somme + pow((donnes[i] - moyenne), 2);
    }
    return somme / (nb_donness - 1);
    
}

/**
 * @brief permet de calculer un intervalle de confiance à 99% pour la valeur de Pi
 * @param nb_replications nombre de replications de l'expèrience
*/
void intervalleDeConfiance(int nb_replications){
    double moyenne;
    double variance;
    double val_seuil;
    double val_confiance;
    double val_pi_estime[5000];
    double somme_pi_calcules = {0.};

    for (int i = 0; i < nb_replications; i++)
    {
        val_pi_estime[i] = simPi(1000);
        somme_pi_calcules = somme_pi_calcules + val_pi_estime[i];
    }

    if(nb_replications < 30){
        val_seuil = alpha_valeurs[nb_replications - 1];
    }
    else{
        val_seuil = 2.576;
    }

    moyenne = somme_pi_calcules / nb_replications;
    variance = calulerEstimateurVariance(moyenne, val_pi_estime, nb_replications);
    val_confiance = val_seuil * sqrt(variance / nb_replications);

    printf("Pour %d réplications :\n", nb_replications);
    printf("Moyenne de PI : %f \n", moyenne);
    printf("Variance : %f \n", variance);
    printf("Intervalle de confiance à 99%% [%f - %f]\n\n", moyenne - val_confiance, moyenne + val_confiance);
}

/**
 * @brief test du focntionnment du calcul de l'intervalle de confiance
*/
void testInteravalleDeConfiance(){
    int nb_repetitions[14] = {10, 200, 600, 1000, 1400, 1800, 2200, 2600, 3000, 3400, 3800, 4200, 4600, 5000};

    //printf("Saisir le nombre de repetitions : \n");
    //scanf("%d", &nb_repetitions);
    for(int i = 0; i < 14; i++){
        intervalleDeConfiance(nb_repetitions[i]);
    }
}

/**
 * @brief Réponse à la question 3
*/
void question3(){
    printf(ANSI_COLOR_SKYBLUE "\nQuestion 3 : \n" ANSI_COLOR_RESET);

    testInteravalleDeConfiance();
}

