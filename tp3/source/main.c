#include <math.h>

#include "matsumoto.h"
#include "global.h"
#include "question1.h"
#include "question2.h"
#include "question3.h"



int main(){
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    question1();

    question2();

    question3();

}