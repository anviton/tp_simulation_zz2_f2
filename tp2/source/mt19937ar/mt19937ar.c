/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)  
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

#include <stdio.h>

//ajout Antoine
#include <math.h>

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* initializes mt[N] with a seed */
void init_genrand(unsigned long s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] = 
	    (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array(unsigned long init_key[], int key_length)
{
    int i, j, k;
    init_genrand(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32(void)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }
  
    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long genrand_int31(void)
{
    return (long)(genrand_int32()>>1);
}

/* generates a random number on [0,1]-real-interval */
double genrand_real1(void)
{
    return genrand_int32()*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double genrand_real2(void)
{
    return genrand_int32()*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double genrand_real3(void)
{
    return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53(void) 
{ 
    unsigned long a=genrand_int32()>>5, b=genrand_int32()>>6; 
    return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 
/* These real versions are due to Isaku Wada, 2002/01/09 added */

/**
 * @brief 
*/
double uniform(double a, double b){
    double r;
    r = genrand_real1();
    return a + (b - a) * r;
}


void affichage_pour_présentation_des_résultats(int *tab, int taille){
    int i;
    for(i = 0; i < taille; i++){
        printf("%d\n", tab[i]);
    }
}


/**
 * 
*/
void cration_repartition(int nb_indiv){
    double seuil_a = 0.5;
    double seuil_b = 0.65;
    double rd;
    int i, classe_a = 0, classe_b = 0, classe_c = 0;
    for(i = 0; i < nb_indiv; i++){
        rd = uniform(0, 1);
        if(rd < seuil_a){
            classe_a++;
        }
        else{
            if(rd < seuil_b){
                classe_b++;
            }
            else{
                classe_c++;
            }
        }
    }
    printf("Répartion obtenu pour %d : \n", nb_indiv);
    printf("\t Classe A : %10.8f\n", classe_a/(double)nb_indiv);
    printf("\t Classe B : %10.8f\n", classe_b/(double)nb_indiv);
    printf("\t Classe C : %10.8f\n", classe_c/(double)nb_indiv);

}

/**
 * 
*/
void repartition(int *classes, double *cumu_proba, int nb_individu, int nb_classes){
    int i, verif_classe = 1, j;
    double alea;
    for(i = 0; i < nb_classes; i++){
        classes[i] = 0;
    }
    for(i = 0; i < nb_individu; i++){
        j = 0;
        alea = uniform(0, 1);
        while (verif_classe == 1)
        {
            //printf("%f et %f \n", alea, cumu_proba[j]);
            if(alea < cumu_proba[j]){
                classes[j] = classes[j] +  1;
                verif_classe = 0;
                //printf("Classe [%d] : %d\n", j, classes[j]);
            }
            j++;
        }
        verif_classe = 1;
    }
    
}

/**
 * @brief calcul de probabilités
*/
void calculer_proba(double *proba, int nb_classes, int *classes_obs, int nb_individus){
    int i;
    for(i = 0; i < nb_classes; i++){
        proba[i] = classes_obs[i] / (double)nb_individus;
    }
}


/**
 * @brief calcul les proba cumulées
*/
void calcul_proba_cumulee(double *proba, double *cumu_proba, int nb_classes){
    int i;
    cumu_proba[0] = proba[0];
    for ( i = 1; i < nb_classes; i++)
    {
        cumu_proba[i] = cumu_proba[i-1] + proba[i];
    }
    
}

void affichage_repartion(int *classes, char *classes_nom, int nb_classes){
    int i;
    printf("Repartion :\n");
    for (i = 0; i < nb_classes; i++){
        printf("Classe %c : %d\n", classes_nom[i], classes[i]);
    }
}

void test_repartition(void){
    int classes_obs[6];
    int nb_individu = 1800;
    double proba[6];
    double proba_cumu[6];
    int nb_classes = 6;
    char classes_nom[6];

    /*Intialisation données observées*/
    classes_obs[0] = 100;
    classes_obs[1] = 400;
    classes_obs[2] = 600;
    classes_obs[3] = 400;
    classes_obs[4] = 100;
    classes_obs[5] = 200;
    int classes_simu[6];

    calculer_proba(proba, nb_classes, classes_obs, nb_individu);

    classes_nom[0] = 'A';
    classes_nom[1] = 'B';
    classes_nom[2] = 'C';
    classes_nom[3] = 'D';
    classes_nom[4] = 'E';
    classes_nom[5] = 'F';
    /*Fin init*/

    calcul_proba_cumulee(proba, proba_cumu, nb_classes);
    for(int i = 0; i < 6; i ++){
        printf("Proba : %f\n", proba[i]);
        printf("Proba cumulée: %f\n", proba_cumu[i]);
    }

    printf("\nRépartion avec 1000 individus \n");
    repartition(classes_simu, proba_cumu, 1000, nb_classes);
    affichage_repartion(classes_simu, classes_nom, nb_classes);

    printf("\nRépartion avec 1000000 individus \n");
    repartition(classes_simu, proba_cumu, 1000000, nb_classes);
    affichage_repartion(classes_simu, classes_nom, nb_classes);


}

/**
 * @brief
 * 
*/
double negExp(double mean){
    double r = genrand_real2();
    return -mean * log(1.0 - r);
}


double exp_negExp(int nb_opp, double mean){
    int i;
    double somme = 0;
    for(i = 0; i < nb_opp; i++){
        somme = somme + negExp(mean);
    }

    return somme / nb_opp;
}


void test_negExp(void){
    double res;
    res = exp_negExp(1000, 11);
    printf("Le resultat après 1000 oppérations est de : %f\n", res);

    res = exp_negExp(1000000, 11);
    printf("Le resultat après 1000000 oppérations est de : %f\n", res);

}

void discrete_distribution_negExp(int nb_opp, double mean){
    int i;
    double r;
    int tab_repartition[20];

    for ( i = 0; i < 20; i++)
    {
        tab_repartition[i] = 0;
    }
    
    for(i = 0; i < nb_opp; i++){
        r = negExp(mean);
        if(r < 20.0){
            tab_repartition[(int) r]++;
        }
    }

    for(i = 0; i < 20; i++){
        printf("de %d à %d : %d éléments\n", i, i+1, tab_repartition[i]);
    }
}

void test_discrete_repartition_negExp(void){
    printf("Pour 1 000 opérations :\n");
    discrete_distribution_negExp(1000, 10);
    printf("Pour 1 000 000 opérations : \n");
    discrete_distribution_negExp(1000000, 10);
}

void common_dice_lance(int nb_opp){
    int tab_res[120];
    int i, j, nb, somme;

    for ( i = 0; i < 120; i++)
    {
        tab_res[i] = 0;
    }

    for(i = 0; i < nb_opp; i++){
        somme = 0;
        for (j = 0; j < 20; j++)
        {
            nb = uniform(1, 6);
            somme = somme + nb;
        }
        tab_res[somme]++;
    }

    for ( i = 0; i < 120; i++)
    {
        printf(" %d : %d \n", i+1, tab_res[i]);
    }
    
}

void test_common_dice_lance(void){
    printf("On répète l'oppération 1 000 fois : \n");
    common_dice_lance(1000);

    printf("On répète l'oppération 1 000 000 fois : \n");
    common_dice_lance(1000000);

}

/**
 * @brief boxMuller 
*/
void boxMuller(double * x1, double * x2)
{
    double r1 = genrand_real1();
    double r2 = genrand_real1();

    *x1 = cos(2 * M_PI * r2) * (sqrt(-2 *log(r1)));
    *x2 = sin(2 * M_PI * r2) * (sqrt(-2 *log(r1)));
}

void repartition_autour_de_0(int nb_opp){
    int i;
    double x1, x2;
    int tab[6] = {0};
    char classes_nom[6];

    for (i = 0; i < nb_opp; i++)
    {
        boxMuller(&x1, &x2);
        if (x1 >= -0.3 && x1 < -0.2) tab[0]++;
        else if (x1 >= -0.2 && x1 < -0.1) tab[1]++;
        else if (x1 >= -0.1 && x1 < 0.0)tab[2]++;
        else if (x1 >= 0.0 && x1 < 0.1) tab[3]++;
        else if (x1 >= 0.1 && x1 < 0.2) tab[4]++;
        else if (x1 >= 0.2 && x1 < 0.3) tab[5]++;
        
    }

    classes_nom[0] = 'A';
    classes_nom[1] = 'B';
    classes_nom[2] = 'C';
    classes_nom[3] = 'D';
    classes_nom[4] = 'E';
    classes_nom[5] = 'F';
    
    affichage_pour_présentation_des_résultats(tab, 6);
    affichage_repartion(tab, classes_nom, 6);
    
}

void test_Box_and_Muller_repartition(void){
    printf("On répète l'oppération 1 000 fois : \n");
    repartition_autour_de_0(1000);

    printf("On répète l'oppération 1 000 000 fois : \n");
    repartition_autour_de_0(1000000);

}


int main(void)
{
    int i;
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    /*printf("1000 outputs of genrand_int32()\n");
    for (i=0; i<1000; i++) {
      printf("%10lu ", genrand_int32());
      if (i%5==4) printf("\n");
    }
    printf("\n1000 outputs of genrand_real2()\n");
    for (i=0; i<1000; i++) {
      printf("%10.8f ", genrand_real2());
      if (i%5==4) printf("\n");
    }*/

    /*double test_unif;

    test_unif = uniform(-89.2, 56.7);
    printf("%10.8f\n", test_unif);*/

    // cration_repartition(1000);
    // cration_repartition(1000000);

    //test_repartition();


    //test_negExp();

    //test_discrete_repartition_negExp();

    //test_common_dice_lance();

    test_Box_and_Muller_repartition();





    return 0;
}
