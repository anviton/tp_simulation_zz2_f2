#ifndef __5_H__
#define __5_H__

void common_dice_lance(int nb_opp);
double calcul_moyenne(int *valeurs, int nb_tirage, int nb_valeurs);
double calcul_ecart_type(int *valeurs, int nb_valeurs, int nb_tirage, double moyenne);
void test_common_dice_lance(void);
void boxMuller(double * x1, double * x2);
void appartenance_intervalle(int *tab, double valeur);
void repartition_autour_de_0(int nb_opp);
void test_Box_and_Muller_repartition(void);
void partie_5(void);


#endif