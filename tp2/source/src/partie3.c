#include <stdio.h>


#include "global.h"
#include "matsumoto.h"
#include "partie2.h"

/**
 * @brief permet de mettre en place une répartition à 3 classes 
 * en préciasant les probabilités d'appartenir à la première et à la seconde classe
 * @param nb_individu nombre d'individus à répartir
 * @param proba_a probabilité pour un individu d'apartenir à la classe a
 * @param proba_b probabilité pour un individu d'apartenir à la classe b  
*/
void creation_repartition(int nb_indiv, double proba_a, double proba_b){
    double seuil_a = proba_a;
    double seuil_b = proba_a + proba_b;
    double rd;
    int i, classe[3] ={0} ;
    double pourcentages[3];
    
    for(i = 0; i < nb_indiv; i++){
        rd = uniform(0, 1);
        if(rd < seuil_a){
            classe[0]++;
        }
        else{
            if(rd < seuil_b){
                classe[1]++;
            }
            else{
                classe[2]++;
            }
        }
    }

    pourcentages[0] = (classe[0] / (double)nb_indiv) * 100;
    pourcentages[1] = (classe[1] / (double)nb_indiv) * 100;
    pourcentages[2] = (classe[2] / (double)nb_indiv) * 100;

    printf("Répartion obtenu pour %d individus : \n", nb_indiv);
    printf("\t Classe A : %10.8f\n", pourcentages[0]);
    printf("\t Classe B : %10.8f\n", pourcentages[1]);
    printf("\t Classe C : %10.8f\n", pourcentages[2]);
    
    //affichage_pour_presentation_des_resultats_pourcentages(pourcentages, 3);
}

/**
 * @brief Réponse à la question (a) de la partie 3
 * 
 */
void question_part3_a(){
    creation_repartition(1000, 0.5, 0.15);
    creation_repartition(1000000, 0.5, 0.15);
}

/**
 * @brief réalisation d'une simulation en se basant sur les données observées qui sont passées en 
 * paramètre
 *
 * @param classes tableau qui contient le nombre d'individu qui va appartenir à chaque classe 
 * une fois la simulation réalisée.
 * @param cumu_proba tableau qui contient les probabilités cumulées
 * @param nb_individu nombre d'individus de la simulation
 * @param nb_classes nombre de classes
 */
void repartition(int *classes, double *cumu_proba, int nb_individu, int nb_classes){
    int i, verif_classe = 1, j;
    double alea;
    for(i = 0; i < nb_classes; i++){
        classes[i] = 0;
    }
    for(i = 0; i < nb_individu; i++){
        j = 0;
        alea = uniform(0, 1);
        while (verif_classe == 1)
        {
            if(alea < cumu_proba[j]){
                classes[j] = classes[j] +  1;
                verif_classe = 0;
            }
            j++;
        }
        verif_classe = 1;
    }
    
}

/**
 * @brief calcule les probabilité d'apartenir à chaque classe à paritr d'informations observées
 * 
 * @param proba tableau qui va contenir les probabilités calculées pour chaque classe
 * @param nb_classes nombre de classes
 * @param classes_obs tableau contenant le nombre d'individus appartenant à chaque classe
 * @param nb_individus nombre d'individus observés
 */
void calculer_proba(double *proba, int nb_classes, int *classes_obs, int nb_individus){
    int i;
    for(i = 0; i < nb_classes; i++){
        proba[i] = classes_obs[i] / (double)nb_individus;
    }
}


/**
 * @brief calcule des probabilités cumulées
 * 
 * @param proba tableau qui contient les probailité d'appartenir à chaque classe
 * @param cumu_proba tableau qui va contenir les probabilités cumulées
 * @param nb_classes nombre de classes
 */
void calcul_proba_cumulee(double *proba, double *cumu_proba, int nb_classes){
    int i;
    cumu_proba[0] = proba[0];
    for ( i = 1; i < nb_classes; i++)
    {
        cumu_proba[i] = cumu_proba[i-1] + proba[i];
    }
    
}

/**
 * @brief permet de tester deux simulations suite à l'initilisation des données observées
 * Remarque : cette fonction aurait pu être sous divisé en plusiers fonctions
 * 
 */
void test_repartition(void){
    int classes_obs[6];
    int nb_individu = 1800;
    double proba[6];
    double proba_cumu[6];
    int nb_classes = 6;
    char classes_nom[6];

    /*Intialisation données observées*/
    classes_obs[0] = 100;
    classes_obs[1] = 400;
    classes_obs[2] = 600;
    classes_obs[3] = 400;
    classes_obs[4] = 100;
    classes_obs[5] = 200;
    int classes_simu[6];

    calculer_proba(proba, nb_classes, classes_obs, nb_individu);

    classes_nom[0] = 'A';
    classes_nom[1] = 'B';
    classes_nom[2] = 'C';
    classes_nom[3] = 'D';
    classes_nom[4] = 'E';
    classes_nom[5] = 'F';
    /*Fin init*/

    calcul_proba_cumulee(proba, proba_cumu, nb_classes);

    printf("\nRépartion avec 1 000 individus \n");
    repartition(classes_simu, proba_cumu, 1000, nb_classes);
    affichage_repartion(classes_simu, classes_nom, nb_classes);

    //affichage_pour_presentation_des_resultats_effectifs(classes_simu, 6);

    printf("\nRépartion avec 1 000 000 individus \n");
    repartition(classes_simu, proba_cumu, 1000000, nb_classes);
    affichage_repartion(classes_simu, classes_nom, nb_classes);

    //affichage_pour_presentation_des_resultats_effectifs(classes_simu, 6);
}
/**
 * @brief Réponse à la question b
 * 
 */
void question_part3_b() {
    test_repartition();
}

/**
 * @brief Réponse à la partie 3
 * 
 */
void partie_3(){
    printf(ANSI_COLOR_SKYBLUE "\nPartie 3 : \n" ANSI_COLOR_RESET);

    printf(ANSI_COLOR_GREEN "\n Question a :\n\n" ANSI_COLOR_RESET);
    question_part3_a();

    printf(ANSI_COLOR_GREEN "\n Question b :\n\n" ANSI_COLOR_RESET);
    question_part3_b();
}




