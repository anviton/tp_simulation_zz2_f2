#include <stdio.h>

#include "partie2.h"
#include "partie5.h"
#include "global.h"
#include "matsumoto.h"



/**
 * @brief permet de répetter une expérience un certain nombre de fois
 *  * (l'expérience étant lancer un dé 20 fois)
 * @param nb_opp nombre deux fois que l'on va répéter l'expèrience 
*/
void common_dice_lance(int nb_opp){
    int tab_res[120];
    int i, j, nb, somme;
    double moyenne, ecart_type;

    for ( i = 0; i < 120; i++)
    {
        tab_res[i] = 0;
    }

    for(i = 0; i < nb_opp; i++){
        somme = 0;
        for (j = 0; j < 20; j++)
        {
            nb = uniform(1, 6);
            somme = somme + nb;
        }
        tab_res[somme]++;
    }

    for ( i = 0; i < 120; i++)
    {
        printf(" %d : %d \n", i+1, tab_res[i]);
    }
    
    moyenne = calcul_moyenne(tab_res, nb_opp, 120);
    ecart_type = calcul_ecart_type(tab_res, 120, nb_opp, moyenne);

    printf("La moyenne est %10.8f\n", moyenne);
    printf("L'écart type est %10.8f\n", ecart_type);

    //affichage_pour_presentation_des_resultats_effectifs(tab_res, 120);
}

/**
 * @brief calcul la moyenne d'un tableau d'entiers passé en paramètre
 * @param valeurs tableau des valeurs dont l'on souhaite calculer la moyenne
 * @return la moyenne calculée
*/
double calcul_moyenne(int *valeurs, int nb_tirage, int nb_valeurs){
    int i, somme = 0;
    double moyenne;
    for(i = 0; i < nb_valeurs; i++){
        somme = somme + ((i + 1) * valeurs[i]);
    }
    moyenne = (double)somme / nb_tirage;
    return moyenne;
}

/**
 * @brief calcul de l'ecart type à partir d'un tableau d'entiers passé en paramètre et 
 * de la moyenne de ce tableau
 * @param valeurs tableau d'entiers
 * @param moyenne moyenne des valeurs du tableau
 * @return ecart type calculé
*/
double calcul_ecart_type(int *valeurs, int nb_valeurs, int nb_tirage, double moyenne){
    double ecart_type, somme = 0;
    int i;
    for(i = 0; i < nb_valeurs; i++){
        somme = somme + valeurs[i] * pow(((i + 1)  - moyenne), 2);
    }

    ecart_type = sqrt(somme / nb_tirage);
    return ecart_type;
}

/**
 * @brief test de de distribution avec 1 000 et 1 000 000 de répétitions
*/
void test_common_dice_lance(void){
    printf("\nOn répète l'oppération 1 000 fois : \n");
    common_dice_lance(1000);

    printf("\nOn répète l'oppération 1 000 000 fois : \n");
    common_dice_lance(1000000);

}

/**
 * @brief fonction boxMuller permet de générer deux nombres aléatoires
 * @param x1 pointeur sur le premier nombre aléatoire
 * @param x2 pointeur sur le second nombre aléatoire
*/
void boxMuller(double * x1, double * x2){
    double r1 = genrand_real1();
    double r2 = genrand_real1();

    *x1 = cos(2 * M_PI * r2) * (sqrt(-2 *log(r1)));
    *x2 = sin(2 * M_PI * r2) * (sqrt(-2 *log(r1)));
}

/**
 * @brief vérifie l'appartenance de la valeur à un intervalle
 * 
*/
void appartenance_intervalle(int *tab, double valeur){
 if (valeur >= -1.0 && valeur < -0.9) tab[0]++;
else if (valeur >= -0.9 && valeur < -0.8) tab[1]++;
else if (valeur >= -0.8 && valeur < -0.7) tab[2]++;
else if (valeur >= -0.7 && valeur < -0.6) tab[3]++;
else if (valeur >= -0.6 && valeur < -0.5) tab[4]++;
else if (valeur >= -0.5 && valeur < -0.4) tab[5]++;
else if (valeur >= -0.4 && valeur < -0.3) tab[6]++;
else if (valeur >= -0.3 && valeur < -0.2) tab[7]++;
else if (valeur >= -0.2 && valeur < -0.1) tab[8]++;
else if (valeur >= -0.1 && valeur < 0.0) tab[9]++;
else if (valeur >= 0.0 && valeur < 0.1) tab[10]++;
else if (valeur >= 0.1 && valeur < 0.2) tab[11]++;
else if (valeur >= 0.2 && valeur < 0.3) tab[12]++;
else if (valeur >= 0.3 && valeur < 0.4) tab[13]++;
else if (valeur >= 0.4 && valeur < 0.5) tab[14]++;
else if (valeur >= 0.5 && valeur < 0.6) tab[15]++;
else if (valeur >= 0.6 && valeur < 0.7) tab[16]++;
else if (valeur >= 0.7 && valeur < 0.8) tab[17]++;
else if (valeur >= 0.8 && valeur < 0.9) tab[18]++;
else if (valeur >= 0.9 && valeur <= 1.0) tab[19]++;
}

/**
 * @brief permet d'illustrer la répartion des nombres aléatoires générés avec Box Muller entre 0 et 1
 * @param nb_opp nombre de tirage
*/
void repartition_autour_de_0(int nb_opp){
    int i;
    double x1, x2;
    int tab[20] = {0};
    char classes_nom[20];

    for (i = 0; i < nb_opp; i++)
    {
        boxMuller(&x1, &x2);
        appartenance_intervalle(tab, x1);
        appartenance_intervalle(tab, x2);
    }

    classes_nom[0] = 'A';
    classes_nom[1] = 'B';
    classes_nom[2] = 'C';
    classes_nom[3] = 'D';
    classes_nom[4] = 'E';
    classes_nom[5] = 'F';
    classes_nom[6] = 'G';
    classes_nom[7] = 'H';
    classes_nom[8] = 'I';
    classes_nom[9] = 'J';
    classes_nom[10] = 'K';
    classes_nom[11] = 'L';
    classes_nom[12] = 'M';
    classes_nom[13] = 'N';
    classes_nom[14] = 'O';
    classes_nom[15] = 'P';
    classes_nom[16] = 'Q';
    classes_nom[17] = 'R';
    classes_nom[18] = 'S';
    classes_nom[19] = 'T';
    
    //affichage_pour_presentation_des_resultats_effectifs(tab, 20);
    affichage_repartion(tab, classes_nom, 20);
    
}

/**
 * @brief test de la répartion autour de 0 avec Box Muller pour 1 000 et 1 000 000 d'individus
*/
void test_Box_and_Muller_repartition(void){
    printf("\nOn répète l'oppération 1 000 fois : \n");
    repartition_autour_de_0(1000);

    printf("\nOn répète l'oppération 1 000 000 fois : \n");
    repartition_autour_de_0(1000000);

}

/**
 * @brief réponse à la partie 5
*/
void partie_5(void){
    printf(ANSI_COLOR_SKYBLUE "\nPartie 5 : \n" ANSI_COLOR_RESET);

    printf(ANSI_COLOR_GREEN "\n Question common dice :\n\n" ANSI_COLOR_RESET);
    test_common_dice_lance();

    printf(ANSI_COLOR_GREEN "\n Question Box Muller :\n\n" ANSI_COLOR_RESET);
    test_Box_and_Muller_repartition();
}