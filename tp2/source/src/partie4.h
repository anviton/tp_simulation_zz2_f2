#ifndef __4_H__
#define __4_H__

double negExp(double mean);
double exp_negExp(int nb_opp, double mean);
void question_part4_b(void);
void discrete_distribution_negExp(int nb_opp, double mean);
void question_part4_c(void);
void partie_4(void);

#endif