#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_SKYBLUE "\x1b[96;1m"
#define ANSI_COLOR_GREEN "\x1b[32;1m"
#define ANSI_COLOR_ORANGE "\x1b[33;1m"
#define ANSI_COLOR_YELLOW "\x1b[93;1m"

void affichage_pour_presentation_des_resultats_effectifs(int *tab, int taille);
void affichage_pour_presentation_des_resultats_pourcentages(double *tab, int taille);
void affichage_repartion(int *classes, char *classes_nom, int nb_classes);

#endif