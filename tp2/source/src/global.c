#include <stdio.h>

/**
 * @brief permet d'afficher le contenu d'un tableau en colonne dans le terminal pour utiliser les données facilement dans 
 * un tableaur pour générer un graphique
 * 
 * @param tab tableau de données
 * @param taille tialle du tableau
 */
void affichage_pour_presentation_des_resultats_effectifs(int *tab, int taille){
    int i;
    for(i = 0; i < taille; i++){
        printf("%d\n", tab[i]);
    }
}

/**
 * @brief permet d'afficher le contenu d'un tableau en colonne dans le terminal pour utiliser les données facilement dans 
 * un tableaur pour générer un graphique
 * 
 * @param tab tableau de données
 * @param taille tialle du tableau
 */
void affichage_pour_presentation_des_resultats_pourcentages(double *tab, int taille){
    int i;
    for(i = 0; i < taille; i++){
        printf("%10.8f\n", tab[i]);
    }
}

/**
 * @brief permet d'afficher pour chaque classe le nombre d'individu qui la compose
 * 
 * @param classes tableau avec la répartion obtenue
 * @param classes_nom tableau contenant le nom de chaque classe
 * @param nb_classes nombre de classes
 */
void affichage_repartion(int *classes, char *classes_nom, int nb_classes){
    int i;
    printf("Repartion :\n");
    for (i = 0; i < nb_classes; i++){
        printf("Classe %c : %d\n", classes_nom[i], classes[i]);
    }
}