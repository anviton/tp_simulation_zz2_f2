#include <stdio.h>


#include "global.h"
#include "matsumoto.h"


/**
 * @brief générer des valeurs aléatoires suivant 
 * une distribution exponentielle négative avec une moyenne donnée en entrée
 * @param mean moyenne donnée en entrée
 * 
*/
double negExp(double mean){
    double r = genrand_real2();
    return -mean * log(1.0 - r);
}

/**
 * @brief calcul la moyenne obtenue en répétant un certain nombre de fois l'appel à negExp
 * @param nb_opp nombre de répétion de l'appel à negExp
 * @return la moyenne obtenue
*/
double exp_negExp(int nb_opp, double mean){
    int i;
    double somme = 0;
    for(i = 0; i < nb_opp; i++){
        somme = somme + negExp(mean);
    }

    return somme / nb_opp;
}

/**
 * @brief reponse à la question b de la partie 4
 * test de la moyenne obtenue en appelant negExp
*/
void question_part4_b(void){
    double res;
    res = exp_negExp(1000, 11);
    printf("Le resultat après 1000 oppérations est de : %f\n", res);

    res = exp_negExp(1000000, 11);
    printf("Le resultat après 1000000 oppérations est de : %f\n", res);

}

/**
 * @brief test de la distrbution discrete de negexp
 * @param nb_opp nombre de tirage effectué
 * @param mean moyenne utilisé pour negExp
 * 
*/
void discrete_distribution_negExp(int nb_opp, double mean){
    int i;
    double r;
    int tab_repartition[20];

    for ( i = 0; i < 20; i++)
    {
        tab_repartition[i] = 0;
    }
    
    for(i = 0; i < nb_opp; i++){
        r = negExp(mean);
        if(r < 20.0){
            tab_repartition[(int) r]++;
        }
    }

    for(i = 0; i < 20; i++){
        printf("de %d à %d : %d éléments\n", i, i+1, tab_repartition[i]);
    }

    //affichage_pour_presentation_des_resultats_effectifs(tab_repartition, 20);
}

/**
 * @brief Réponse à la question c de la partie 4
*/
void question_part4_c(void){
    printf("\n");
    printf("Pour 1 000 opérations :\n");
    discrete_distribution_negExp(1000, 10);
    printf("\n");
    printf("Pour 1 000 000 opérations : \n");
    discrete_distribution_negExp(1000000, 10);
}

/**
 * @brief réponse aux question de la partie 4
*/
void partie_4(void){
    printf(ANSI_COLOR_SKYBLUE "\nPartie 4 : \n" ANSI_COLOR_RESET);

    printf(ANSI_COLOR_GREEN "\n Question b :\n\n" ANSI_COLOR_RESET);
    question_part4_b();

    printf(ANSI_COLOR_GREEN "\n Question c :\n\n" ANSI_COLOR_RESET);
    question_part4_c();
}