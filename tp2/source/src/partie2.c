#include <stdio.h>


#include "global.h"
#include "matsumoto.h"


/**
 * @brief fonction uniform génère un nombre entre a et b
 * @param a borne inférieure 
 * @param b borne supérieure
 * @return le double généré  
*/
double uniform(double a, double b){
    double r;
    r = genrand_real1();
    return a + (b - a) * r;
}

/**
 * @brief Test de la fonction uniform
 * 
 */
void test_uniform(){
    double test_unif;

    test_unif = uniform(-89.2, 56.7);
    printf("Température générée : %10.8f\n", test_unif);
}


/**
 * @brief réponse à la partie 2
 * 
 */
void partie_2(){
    printf(ANSI_COLOR_SKYBLUE "\nPartie 2 : \n" ANSI_COLOR_RESET);
    printf(ANSI_COLOR_GREEN "\n Question unique :\n\n" ANSI_COLOR_RESET);
    test_uniform();
}
